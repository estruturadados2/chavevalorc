#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct t_time{
    char key[9];
    char * val;
    struct t_time * next;
} t_time;

typedef struct t_timetable{
    t_time * node;
} t_timetable;


t_timetable * table;


t_timetable * init(){
    t_timetable * tab = (t_timetable *)malloc(sizeof(t_timetable));
    tab->node = NULL;
    return tab;
}

t_time init_t_time(char key[8], char * val){
    t_time tm;
    strcpy(tm.key, key);
    tm.val = val;
    return tm;
}

int time_cmp(struct t_time * a, struct t_time * b){
    return(strcmp(a->key, b->key));
}

int contains(t_time key){
    t_time *p = table->node;
    t_time *tmp = table->node;

    if(time_cmp(&key, p) == 0){
        return 1;
    }

   while(tmp != NULL){
        if(time_cmp(&key, tmp) == 0){
            return 1;
        }
        tmp = tmp->next;
   }

   return 0;
}

void put(char key[8], char * val){
    t_time *p = table->node;
    t_time *tmp = table->node;

    t_time * var = (t_time *)(malloc(sizeof(t_time)));
    strcpy(var->key, key);
    var->val = val;



    if(table->node == NULL || strcmp(tmp->key, var->key) == -1){
        var->next = table->node;
        table->node = var;
    }else{
        while(tmp != NULL && strcmp(tmp->key, var->key) == 1){
            p = tmp;
            tmp=tmp->next;
        }
        var->next = p->next;
        p->next = var;
    }
}

char * get(t_time key){
    t_timetable * cs = table;
    for(;cs;){
        if(time_cmp(&key, cs->node) == 0){
            return(cs->node->val);
        }
    }

    return NULL;
}

void delete(t_time key){
    t_time *p = table->node;
    t_time *tmp = table->node;

    if(time_cmp(&key, p) == 0){
        table->node = p->next;
        free(p);
        return;
    }

   while(tmp != NULL){
        if(time_cmp(&key, tmp) == 0){
            p->next = tmp->next;
            free(tmp);
            return;
        }
        p = tmp;
        tmp = tmp->next;
   }
}

int is_empty(){
    if(table->node == NULL){
        return 1;
    }
    return 0;
}

int size(){
    int count = 0;
    t_time *tmp = table->node;

    if(table->node == NULL){
        return 0;
    }

    while(tmp != NULL){
        count++;
        tmp = tmp->next;
    }
    return count;
}

t_time * max(){
    return table->node;
}

t_time * min(){
    t_time *tmp = table->node;
    t_time *p = table->node;

    while(tmp != NULL){
        p = tmp;
        tmp = tmp->next;
    }

    return p;
}

void delete_min(){
    t_time *tmp = table->node;
    t_time *p = table->node;

    if(is_empty()){return;}

    if(tmp->next == NULL){
        table->node = NULL;
        free(tmp);
        return;
    }

    while(tmp->next != NULL){
        p = tmp;
        tmp = tmp->next;
    }
    p->next = NULL;
    free(tmp);
}

void delete_max(){
    t_time *tmp = table->node;
    table->node = tmp->next;
    free(tmp);
}

t_time * floor(t_time key){
    t_time *tmp = table->node;
    int c = 0;

    while(tmp != NULL){
        c = time_cmp(tmp, &key);

        if(c == -1 || c == 0){
            return tmp;
        }
        tmp = tmp->next;
    }
    return NULL;
}

t_time * ceiling(t_time key){
    t_time *tmp = table->node;
    t_time *p = table->node;
    int c = 0;

    while(tmp != NULL){
        c = time_cmp(tmp, &key);
        //printf("\n %d %s", c, tmp->key);
        if(c == -1 || c == 0){
            if(c == 0){
                    return tmp;
            }
            return p;
        }
        p = tmp;
        tmp = tmp->next;
    }
    return NULL;
}

t_time * select(int k){
    int count = 0;
    t_time *tmp = table->node;

    while(tmp != NULL){
        if(count == k){
            return tmp;
        }
        count++;
        tmp = tmp->next;
    }
    return NULL;
}

int size_range(t_time lo, t_time hi){
    t_time *tmp = floor(lo);
    t_time *p = floor(hi);
    int count = 1;

    if(tmp == NULL && p == NULL){
        return 0;
    }

    while(time_cmp(tmp, p) != 0){
        //printf("\n\n%s\n", tmp->val);
        count++;
        p = p->next;
    }

    return count;
}

t_time * keys(t_time lo, t_time hi){

    t_time *lo2 = floor(lo);
    t_time *hi2 = floor(hi);

    t_time *tmp = hi2;

    int size = size_range(lo, hi);

    t_time * array = (t_time *) malloc(size*sizeof(t_time));

    int count = 0;
    //printf("\n\n%d\n", size);

    do{
        array[count++] = *hi2;
        tmp = hi2;
        //printf("\n\n rerer %s %s\n", lo2->val, hi2->val);
        hi2 = hi2->next;
    }while(time_cmp(lo2, tmp) != 0);


    return array;
}

void printarLista(){
    t_time *tmp = table->node;
    while(tmp != NULL){
        printf("\n%s", tmp->val);
        tmp = tmp->next;
    }
}


void main(){

    table = init();

    put("09:08:22", "Brasil");
    put("09:03:22", "BR");
    put("09:04:22", "Arg");
    put("09:09:22", "TEstet");
    put("09:03:23", "dad");

    //delete_min();
    //int v = contains(init_t_time("09:04:32", "Arg"));
    //printarLista();
    //printf("\n\n%s\n", keys(init_t_time("09:03:23", "Arg"), init_t_time("09:09:22", "Arg"))[1].val);
}
